Based on official tomcat alpine image with additional keycloak adapter files

This image contains:
- java8
- Apache Tomcat 8.5.*
- Keycloak 3.2.1.final

## Using
1. To start container with shell run ```bin/start_shell.sh```

2. To start tomcat container by hand use a call like this
```bash
# maps container http port 8080 to host port 8000 and container ajp port 8009 to host port 8001
docker run -d --name "myTestTomcat" --cpuset-cpus=0-2 \
  -p 8000:8080 \
  -p 8001:8009 \
  -v HOST_WEBAPPS_DIR:/opt/tomcat/webapps \
  schlothauer/d_tomcat8_keycloak:8.5-keycloak-3.2.1
```

3. How to mount own webapps in the container?
Mount a directory of the local host on the container path '/opt/tomcat/webapps'

4. How add additional webapp configurations to the image?
This feature is optional.
Mount the host directory that contains the needed xml configurations to
the container path '/opt/extraConf'

5. How to add additional libaries to $CATALINA_HOME/lib?
This feature is optional.
Mount a host directory that contains the needed libraries to container path
'/opt/extraLibs'. While startup 'startTomcat.sh' will copy the libs.

6. How to start tomcat in Debug mode
Pass a environment variable DEBUG=1 at container start to the system.

To get a better understanding of the configuration and startup steps, take a look
at '/startTomcat.sh'



[More information here](https://keycloak.gitbooks.io/securing-client-applications-guide/content/topics/oidc/java/tomcat-adapter.html)




