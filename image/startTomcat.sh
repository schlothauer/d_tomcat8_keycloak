#!/bin/sh

for f in /opt/extraConf/*; do
    case "$f" in
    *.xml)    echo "$0: copying $f"; cp "$f" /opt/tomcat/conf/Catalina/localhost ;;
    *)        echo "$0: ignoring $f" ;;
    esac
    echo
done

for f in /opt/extraLibs/*; do
    case "$f" in
    *.jar)    echo "$0: copying $f"; cp "$f" /usr/local/tomcat/lib ;;
    *)        echo "$0: ignoring $f" ;;
    esac
    echo
done

for f in /opt/extraTomcatConf/*; do
    case "$f" in
    *)    echo "$0: copying $f"; cp "$f" /opt/tomcat/conf ;;
    esac
    echo
done


export JAVA_OPTS="-Dfile.encoding=utf-8"

if ! [ -z "$DEBUG" ] ; then
    JPDA_TRANSPORT="dt_socket"
    JPDA_ADDRESS="8000"
    JPDA_SUSPEND="n"
    JPDA_OPTS="-agentlib:jdwp=transport=$JPDA_TRANSPORT,address=$JPDA_ADDRESS,server=y,suspend=$JPDA_SUSPEND"
    export CATALINA_OPTS="$JPDA_OPTS $CATALINA_OPTS"
fi

if ! [ -z $TRUSTSTORE_FILE ]; then
    if [ -f $TRUSTSTORE_FILE ]; then
        export JAVA_OPTS="-Djavax.net.ssl.trustStore=$TRUSTSTORE_FILE" "-Djavax.net.ssl.trustStorePassword=$TRUSTSTORE_PASSWORD"
    fi
fi
export CATALINA_BASE=/opt/tomcat

/usr/local/tomcat/bin/catalina.sh run

